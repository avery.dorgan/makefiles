tg-init-iam-apply:
	terragrunt run-all apply --terragrunt-non-interactive --terragrunt-working-dir terraform/live/global/iam/ecs \
	--terragrunt-ignore-external-dependencies \
	-auto-approve \
	-lock-timeout=10m

tg-init-access-layer-refresh:
	terragrunt apply --terragrunt-non-interactive --terragrunt-working-dir us-east-1/_access-layer \
	-auto-approve \
	-lock-timeout=10m

tg-validate:
	terragrunt run-all validate --terragrunt-non-interactive --terragrunt-working-dir terraform/live \
	--terragrunt-exclude-dir **/monitoring/**/**

tg-plan:
	terragrunt run-all plan --terragrunt-non-interactive --terragrunt-working-dir terraform/live \
	--terragrunt-exclude-dir **/monitoring/**/** \
	-lock-timeout=10m

tg-apply-non-prod:
	cd terraform/live && \
	terragrunt run-all apply \
	--terragrunt-non-interactive \
	--terragrunt-exclude-dir **/backup -auto-approve -lock-timeout=10m

tg-apply:
	terragrunt run-all apply --terragrunt-non-interactive --terragrunt-working-dir terraform/live \
	--terragrunt-exclude-dir **/monitoring/**/** \
	-lock-timeout=10m \
	-auto-approve

tg-plan-monitoring:
	terragrunt run-all plan --terragrunt-non-interactive --terragrunt-working-dir terraform/live/monitoring \
	-lock-timeout=10m

tg-apply-monitoring:
	terragrunt run-all apply --terragrunt-non-interactive --terragrunt-working-dir terraform/live/monitoring \
	-lock-timeout=10m -auto-approve

tg-upgrade-providers: # Intended for run by a human on a local desktop followed by multi-platform lock generation
	terragrunt run-all init -upgrade --terragrunt-working-dir terraform/live/

tg-providers-lock: # Intended for run by a human on a local desktop followed by copy/paste of highest-common-version into root terragrunt.hcl
	terragrunt run-all providers lock --terragrunt-ignore-external-dependencies --terragrunt-working-dir terraform/live/ \
			-platform=windows_amd64 \
			-platform=darwin_amd64 \
			-platform=darwin_arm64 \
			-platform=linux_amd64 \
			-platform=linux_arm64
